package com.ebay.test.rock_paper_scissors;

import java.util.Scanner;

import static java.lang.Integer.MAX_VALUE;
import static java.lang.String.format;

public class InputAsker {

    private static final int DEFAULT_TIME_TO_PLAY = 15;
    private static final int DEFAULT_GAME_MODE = 1;

    private final Scanner scanner;

    public InputAsker(Scanner scanner) {
        this.scanner = scanner;
    }

    public int getPlayerOption() {
        System.out.println("Play [1 = Rock, 2 = Paper, 3 = Scissors]: ");

        int userInput = getUserInput(-1, 1, 3);

        if (userInput == -1)
            System.out.println("Option not recognized");

        return userInput;
    }

    public int getTimeToPlay() {
        System.out.println(format("How long do you want to play for (in seconds, default is %d, minimum is 3)?", DEFAULT_TIME_TO_PLAY));

        int timeToPlay = getUserInput(DEFAULT_TIME_TO_PLAY, 3, MAX_VALUE);

        System.out.println(format("You will play for %d seconds", timeToPlay));

        return timeToPlay;
    }

    public int getGameMode() {
        System.out.println("Choose [1 = Player vs Computer, 2 = Computer vs Computer]: ");

        int gameMode = getUserInput(DEFAULT_GAME_MODE, 1, 2);

        if (gameMode == 2)
            System.out.println("You will play: Computer vs Computer");
        else
            System.out.println("You will play: Player vs Computer");

        return gameMode;
    }

    private int getUserInput(int defaultValue, int minAllowed, int maxAllowed) {
        int result = defaultValue;

        if (scanner.hasNextInt()) {
            int value = scanner.nextInt();
            if (value >= minAllowed && value <= maxAllowed)
                result = value;
        } else
            scanner.next();
        return result;
    }
}
