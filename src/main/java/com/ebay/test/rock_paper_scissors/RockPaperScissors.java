package com.ebay.test.rock_paper_scissors;

import static java.lang.String.format;

public class RockPaperScissors {

    enum OPTION {
        ROCK {
            @Override
            public boolean beats(OPTION other) {
                return other == SCISSORS;
            }
        },
        PAPER {
            @Override
            public boolean beats(OPTION other) {
                return other == ROCK;
            }
        },
        SCISSORS {
            @Override
            public boolean beats(OPTION other) {
                return other == PAPER;
            }
        };

        public abstract boolean beats(OPTION other);
    }

    public static OPTION calculate(OPTION option1, OPTION option2) {
        try {
            if (option1 == null || option2 == null)
                throw new RuntimeException();

            if (option1 == option2)
                System.out.println("It's a tie");
            else if (option1.beats(option2)) {
                System.out.println(format("%s beats %s", option1, option2));
                return option1;
            } else {
                System.out.println(format("%s beats %s", option2, option1));
                return option2;
            }
        } catch (RuntimeException e) {
            System.out.println("Unable to determine winner");
        }

        return null;
    }

}
