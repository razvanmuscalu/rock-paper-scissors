package com.ebay.test.rock_paper_scissors;

import com.ebay.test.rock_paper_scissors.RockPaperScissors.OPTION;

import java.util.concurrent.Callable;

public abstract class Player implements Callable<OPTION> {

    private long currentScore;
    protected final String name;

    public Player(String name) {
        this.currentScore = 0;
        this.name = name;
    }

    public void incrementCurrentScore() {
        this.currentScore = currentScore + 1;
    }

    public long getCurrentScore() {
        return currentScore;
    }

    public String getName() {
        return name;
    }
}
