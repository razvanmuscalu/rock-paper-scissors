package com.ebay.test.rock_paper_scissors;

import com.ebay.test.rock_paper_scissors.RockPaperScissors.OPTION;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import static com.ebay.test.rock_paper_scissors.RockPaperScissors.OPTION.values;
import static java.lang.String.format;

public class HumanPlayer extends Player {
    private final CyclicBarrier barrier;
    private final InputAsker inputAsker;

    public HumanPlayer(CyclicBarrier barrier, String name, InputAsker inputAsker) {
        super(name);
        this.barrier = barrier;
        this.inputAsker = inputAsker;
    }

    @Override
    public OPTION call() {
        int playerOption = inputAsker.getPlayerOption();

        if (playerOption != -1) {
            OPTION option = values()[playerOption - 1];

            System.out.println(format("%s played", name));

            try {
                barrier.await();
            } catch (BrokenBarrierException | InterruptedException exception) {
                System.out.println("An exception occurred while waiting... " + exception);
            }

            return option;
        }

        return null;
    }
}
