package com.ebay.test.rock_paper_scissors;

import com.ebay.test.rock_paper_scissors.RockPaperScissors.OPTION;

import java.util.concurrent.*;

import static com.ebay.test.rock_paper_scissors.RockPaperScissors.calculate;
import static java.lang.String.format;
import static java.lang.System.currentTimeMillis;
import static java.lang.Thread.sleep;
import static java.util.concurrent.Executors.newFixedThreadPool;

public class GameService {

    private final InputAsker inputAsker;

    public GameService(InputAsker inputAsker) {
        this.inputAsker = inputAsker;
    }

    public void start() {
        int gameMode = inputAsker.getGameMode();
        int timeToPlay = inputAsker.getTimeToPlay();

        CyclicBarrier barrier = new CyclicBarrier(2);
        Player player1 = getPlayer1(gameMode, barrier);
        Player player2 = new ComputerPlayer(barrier, "Computer");

        play(timeToPlay, player1, player2);
    }

    private Player getPlayer1(int gameMode, CyclicBarrier barrier) {
        if (gameMode == 1)
            return new HumanPlayer(barrier, "Player", inputAsker);
        else
            return new ComputerPlayer(barrier, "Computer 1");
    }

    private void play(int timeToPlay, Player player1, Player player2) {
        ExecutorService executor = newFixedThreadPool(2);

        long startTime = currentTimeMillis();
        while (true) {
            if ((currentTimeMillis() - startTime) < timeToPlay * 1000) {
                countDown();
                playTurn(executor, player1, player2);
            } else {
                System.out.println("======= FINAL SCORE =======");
                System.out.println(format("%s: %d vs %s: %d", player1.getName(), player1.getCurrentScore(), player2.getName(), player2.getCurrentScore()));
                System.exit(0);
            }
        }
    }

    private void countDown() {
        CountDownLatch latch = new CountDownLatch(3);

        for (int i = 3; i > 0; i--) {
            try {
                sleep(1_000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(i);
            latch.countDown();
        }
    }

    private void playTurn(ExecutorService executor, Player player1, Player player2) {
        try {

            Future<OPTION> player1OptionFuture = executor.submit(player1);
            Future<OPTION> player2OptionFuture = executor.submit(player2);

            OPTION player1Option = player1OptionFuture.get(3, TimeUnit.SECONDS);
            OPTION player2Option = player2OptionFuture.get(3, TimeUnit.SECONDS);

            OPTION winner = calculate(player1Option, player2Option);

            if (winner == player1Option)
                player1.incrementCurrentScore();
            else if (winner == player2Option)
                player2.incrementCurrentScore();
        } catch (RejectedExecutionException ignored) {
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            System.out.println(format("Time is up, %s loses", player1.getName()));
            player2.incrementCurrentScore();
        }
    }

}
