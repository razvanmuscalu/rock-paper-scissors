package com.ebay.test.rock_paper_scissors;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        InputAsker inputAsker = new InputAsker(scanner);
        GameService gameService = new GameService(inputAsker);

        gameService.start();
    }
}
