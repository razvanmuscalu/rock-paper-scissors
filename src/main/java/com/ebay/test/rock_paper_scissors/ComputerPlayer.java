package com.ebay.test.rock_paper_scissors;

import com.ebay.test.rock_paper_scissors.RockPaperScissors.OPTION;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import static com.ebay.test.rock_paper_scissors.RockPaperScissors.OPTION.values;
import static java.lang.String.format;

public class ComputerPlayer extends Player {
    private final CyclicBarrier barrier;

    public ComputerPlayer(CyclicBarrier barrier, String name) {
        super(name);
        this.barrier = barrier;
    }

    @Override
    public OPTION call() {
        OPTION selectedOption = values()[new Random().nextInt(values().length)];

        System.out.println(format("%s played", name));

        try {
            barrier.await();
        } catch (BrokenBarrierException | InterruptedException exception) {
            System.out.println("An exception occurred while waiting... " + exception);
        }

        return selectedOption;
    }
}
