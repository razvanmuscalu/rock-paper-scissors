package com.ebay.test.rock_paper_scissors;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

import static java.lang.System.currentTimeMillis;
import static java.lang.System.setIn;
import static org.apache.commons.lang3.StringUtils.countMatches;
import static org.apache.commons.lang3.StringUtils.substring;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.data.Percentage.withPercentage;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GameServiceTest {

    @Mock
    private InputAsker inputAsker;

    @InjectMocks
    private GameService sut;

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Test
    public void shouldBeAbleToPlayComputerVsComputer() {
        when(inputAsker.getGameMode()).thenReturn(2);
        when(inputAsker.getTimeToPlay()).thenReturn(1);

        exit.expectSystemExitWithStatus(0);
        exit.checkAssertionAfterwards(() -> {
            assertThat(systemOutRule.getLog()).contains("Computer played");
            assertThat(systemOutRule.getLog()).contains("Computer 1 played");
        });

        sut.start();
    }

    @Test
    public void shouldBeAbleToPlayPlayerVsComputer() {
        when(inputAsker.getGameMode()).thenReturn(1);
        when(inputAsker.getTimeToPlay()).thenReturn(1);
        when(inputAsker.getPlayerOption()).thenReturn(1);

        exit.expectSystemExitWithStatus(0);
        exit.checkAssertionAfterwards(() -> {
            assertThat(systemOutRule.getLog()).contains("Player played");
            assertThat(systemOutRule.getLog()).contains("Computer played");
        });

        sut.start();
    }

    @Test
    public void shouldPlayForSpecifiedDuration() {
        when(inputAsker.getGameMode()).thenReturn(1);
        when(inputAsker.getTimeToPlay()).thenReturn(30);
        when(inputAsker.getPlayerOption()).thenReturn(1);

        exit.expectSystemExitWithStatus(0);
        long start = currentTimeMillis();
        exit.checkAssertionAfterwards(() -> {
            assertThat(currentTimeMillis() - start).isCloseTo(30_000, withPercentage(25));
        });

        sut.start();
    }

    @Test
    public void shouldTimeoutWhenInputIsNotValid() {
        when(inputAsker.getGameMode()).thenReturn(1);
        when(inputAsker.getTimeToPlay()).thenReturn(5);
        when(inputAsker.getPlayerOption()).thenReturn(-1);

        exit.expectSystemExitWithStatus(0);
        exit.checkAssertionAfterwards(() -> {
            assertThat(systemOutRule.getLog()).contains("Time is up, Player loses");
        });

        sut.start();
    }

    @Test
    public void shouldKeepScore() {
        when(inputAsker.getGameMode()).thenReturn(1);
        when(inputAsker.getTimeToPlay()).thenReturn(20);
        when(inputAsker.getPlayerOption()).thenReturn(1);

        exit.expectSystemExitWithStatus(0);
        exit.checkAssertionAfterwards(() -> {
            String log = systemOutRule.getLog();
            String scoreLine = substring(log, log.indexOf("Player: "));

            int player1Score = new Scanner(scoreLine).useDelimiter("[^0-9]+").nextInt();
            int actualScoreFromLogs = countMatches(log, "ROCK beats");

            assertThat(player1Score).isEqualTo(actualScoreFromLogs);
        });

        sut.start();
    }

    @Test
    public void shouldIncrementComputerScoreWhenPlayerTimesOut() {
        when(inputAsker.getGameMode()).thenReturn(1);
        when(inputAsker.getTimeToPlay()).thenReturn(10);
        when(inputAsker.getPlayerOption()).thenReturn(-1);

        exit.expectSystemExitWithStatus(0);
        exit.checkAssertionAfterwards(() -> {
            assertThat(systemOutRule.getLog()).contains("vs Computer: 2");
        });

        sut.start();
    }

    @Test
    public void shouldCountDown() {
        when(inputAsker.getGameMode()).thenReturn(2);
        when(inputAsker.getTimeToPlay()).thenReturn(5);
        when(inputAsker.getPlayerOption()).thenReturn(1);

        exit.expectSystemExitWithStatus(0);
        exit.checkAssertionAfterwards(() -> {
            assertThat(systemOutRule.getLog()).contains("3\n");
            assertThat(systemOutRule.getLog()).contains("2\n");
            assertThat(systemOutRule.getLog()).contains("1\n");
        });

        sut.start();
    }
}
