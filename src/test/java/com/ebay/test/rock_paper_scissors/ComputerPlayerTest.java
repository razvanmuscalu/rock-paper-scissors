package com.ebay.test.rock_paper_scissors;

import com.ebay.test.rock_paper_scissors.RockPaperScissors.OPTION;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CyclicBarrier;

import static com.ebay.test.rock_paper_scissors.RockPaperScissors.OPTION.PAPER;
import static com.ebay.test.rock_paper_scissors.RockPaperScissors.OPTION.ROCK;
import static com.ebay.test.rock_paper_scissors.RockPaperScissors.OPTION.SCISSORS;
import static java.lang.System.setIn;
import static org.assertj.core.api.Assertions.assertThat;

public class ComputerPlayerTest {

    private final CyclicBarrier barrier = new CyclicBarrier(1);
    private ComputerPlayer sut;

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void shouldPlayRockPaperScissorsRandomly() {
        setIn(new ByteArrayInputStream("1".getBytes()));
        sut = new ComputerPlayer(barrier, "test");

        List<OPTION> results = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            results.add(sut.call());
        }

        assertThat(results).contains(ROCK);
        assertThat(results).contains(PAPER);
        assertThat(results).contains(SCISSORS);
    }

    @Test
    public void shouldInformThatComputerPlayed() {
        setIn(new ByteArrayInputStream("1".getBytes()));
        sut = new ComputerPlayer(barrier, "test");

        sut.call();

        assertThat(systemOutRule.getLog()).contains("test played");
    }
}
