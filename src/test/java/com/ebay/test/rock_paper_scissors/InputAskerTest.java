package com.ebay.test.rock_paper_scissors;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

import static java.lang.System.setIn;
import static org.assertj.core.api.Assertions.assertThat;

public class InputAskerTest {

    private Scanner scanner;
    private InputAsker inputAsker;

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void shouldAskForGameLength() {
        setIn(new ByteArrayInputStream("4".getBytes()));
        scanner = new Scanner(System.in);
        inputAsker = new InputAsker(scanner);

        inputAsker.getTimeToPlay();

        assertThat(systemOutRule.getLog()).contains("How long do you want to play for (in seconds, default is 15, minimum is 3)?");

    }

    @Test
    public void shouldDefaultTo15SecondsTimeToPlay() {
        setIn(new ByteArrayInputStream("invalid".getBytes()));
        scanner = new Scanner(System.in);
        inputAsker = new InputAsker(scanner);

        assertThat(inputAsker.getTimeToPlay()).isEqualTo(15);
        assertThat(systemOutRule.getLog()).contains("You will play for 15 seconds");
    }

    @Test
    public void shouldDefaultTo15SecondsTimeToPlayWhenUserInputsInvalidInteger() {
        setIn(new ByteArrayInputStream("-1".getBytes()));
        scanner = new Scanner(System.in);
        inputAsker = new InputAsker(scanner);

        assertThat(inputAsker.getTimeToPlay()).isEqualTo(15);
        assertThat(systemOutRule.getLog()).contains("You will play for 15 seconds");
    }

    @Test
    public void shouldReturnTimeToPlayFromInput() {
        setIn(new ByteArrayInputStream("10".getBytes()));
        scanner = new Scanner(System.in);
        inputAsker = new InputAsker(scanner);

        assertThat(inputAsker.getTimeToPlay()).isEqualTo(10);
        assertThat(systemOutRule.getLog()).contains("You will play for 10 seconds");
    }

    @Test
    public void shouldAskForGameMode() {
        setIn(new ByteArrayInputStream("4".getBytes()));
        scanner = new Scanner(System.in);
        inputAsker = new InputAsker(scanner);

        inputAsker.getGameMode();

        assertThat(systemOutRule.getLog()).contains("Choose [1 = Player vs Computer, 2 = Computer vs Computer]: ");
    }

    @Test
    public void shouldDefaultToPlayerVsComputerGameMode() {
        setIn(new ByteArrayInputStream("invalid".getBytes()));
        scanner = new Scanner(System.in);
        inputAsker = new InputAsker(scanner);

        assertThat(inputAsker.getGameMode()).isEqualTo(1);
        assertThat(systemOutRule.getLog()).contains("You will play: Player vs Computer");
    }

    @Test
    public void shouldDefaultToPlayerVsComputerGameModeWhenUserInputsInvalidInteger() {
        setIn(new ByteArrayInputStream("3".getBytes()));
        scanner = new Scanner(System.in);
        inputAsker = new InputAsker(scanner);

        assertThat(inputAsker.getGameMode()).isEqualTo(1);
        assertThat(systemOutRule.getLog()).contains("You will play: Player vs Computer");

    }

    @Test
    public void shouldReturnGameModeFromInput() {
        setIn(new ByteArrayInputStream("2".getBytes()));
        scanner = new Scanner(System.in);
        inputAsker = new InputAsker(scanner);

        assertThat(inputAsker.getGameMode()).isEqualTo(2);
        assertThat(systemOutRule.getLog()).contains("You will play: Computer vs Computer");

    }

    @Test
    public void shouldReturnPlayerOptionInput() {
        setIn(new ByteArrayInputStream("2".getBytes()));
        scanner = new Scanner(System.in);
        inputAsker = new InputAsker(scanner);

        assertThat(inputAsker.getPlayerOption()).isEqualTo(2);
    }

    @Test
    public void shouldDefaultWhenUserInputsInvalidInteger() {
        setIn(new ByteArrayInputStream("4".getBytes()));
        scanner = new Scanner(System.in);
        inputAsker = new InputAsker(scanner);

        assertThat(inputAsker.getPlayerOption()).isEqualTo(-1);
    }

    @Test
    public void shouldDefaultWhenUserInputIsNotValid() {
        setIn(new ByteArrayInputStream("invalid".getBytes()));
        scanner = new Scanner(System.in);
        inputAsker = new InputAsker(scanner);

        assertThat(inputAsker.getPlayerOption()).isEqualTo(-1);
    }

    @Test
    public void shouldAskForPlayerOption() {
        setIn(new ByteArrayInputStream("4".getBytes()));
        scanner = new Scanner(System.in);
        inputAsker = new InputAsker(scanner);

        assertThat(inputAsker.getPlayerOption()).isEqualTo(-1);
        assertThat(systemOutRule.getLog()).contains("Play [1 = Rock, 2 = Paper, 3 = Scissors]: ");

    }

    @Test
    public void shouldInformWhenPlayerOptionNotValid() {
        setIn(new ByteArrayInputStream("4".getBytes()));
        scanner = new Scanner(System.in);
        inputAsker = new InputAsker(scanner);

        inputAsker.getPlayerOption();

        assertThat(systemOutRule.getLog()).contains("Option not recognized");
    }
}
