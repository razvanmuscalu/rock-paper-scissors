package com.ebay.test.rock_paper_scissors.integration;

import com.ebay.test.rock_paper_scissors.GameService;
import com.ebay.test.rock_paper_scissors.InputAsker;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.contrib.java.lang.system.SystemOutRule;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

import static java.lang.System.setIn;
import static org.assertj.core.api.Assertions.assertThat;

public class RockPaperScissorsIntegrationTest {

    private Scanner scanner;
    private InputAsker inputAsker;
    private GameService sut;

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Test
    public void shouldHaveToSpecifyGameMode() {
        setIn(new ByteArrayInputStream("1\n3".getBytes()));
        scanner = new Scanner(System.in);
        inputAsker = new InputAsker(scanner);
        sut = new GameService(inputAsker);

        exit.expectSystemExitWithStatus(0);
        exit.checkAssertionAfterwards(() -> {
            assertThat(systemOutRule.getLog()).contains("Choose [1 = Player vs Computer, 2 = Computer vs Computer]: ");
            assertThat(systemOutRule.getLog()).contains("You will play: Player vs Computer");
        });

        sut.start();
    }

    @Test
    public void shouldHaveToSpecifyTimeToPlay() {
        setIn(new ByteArrayInputStream("1\n5".getBytes()));
        scanner = new Scanner(System.in);
        inputAsker = new InputAsker(scanner);
        sut = new GameService(inputAsker);

        exit.expectSystemExitWithStatus(0);
        exit.checkAssertionAfterwards(() -> {
            assertThat(systemOutRule.getLog()).contains("How long do you want to play for (in seconds, default is 15, minimum is 3)?");
            assertThat(systemOutRule.getLog()).contains("You will play for 5 seconds");
        });

        sut.start();
    }

    @Test
    public void shouldBeAbleToPlayADifferentOptionEachTime() {
        setIn(new ByteArrayInputStream("1\n45\n1\n2\n3\n1\n2\n3\n1\n2\n3\n1\n2\n3\n1\n2\n3\n1\n2\n3\n1\n2\n3\n1\n2\n3".getBytes()));
        scanner = new Scanner(System.in);
        inputAsker = new InputAsker(scanner);
        sut = new GameService(inputAsker);

        exit.expectSystemExitWithStatus(0);
        exit.checkAssertionAfterwards(() -> {
            assertThat(systemOutRule.getLog()).contains("Computer played");
            assertThat(systemOutRule.getLog()).contains("Player played");
            assertThat(systemOutRule.getLog()).contains("ROCK beats");
            assertThat(systemOutRule.getLog()).contains("PAPER beats");
            assertThat(systemOutRule.getLog()).contains("SCISSORS beats");
            assertThat(systemOutRule.getLog()).contains("beats ROCK");
            assertThat(systemOutRule.getLog()).contains("beats PAPER");
            assertThat(systemOutRule.getLog()).contains("beats SCISSORS");
        });

        sut.start();
    }
}