package com.ebay.test.rock_paper_scissors;

import com.ebay.test.rock_paper_scissors.RockPaperScissors.OPTION;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static com.ebay.test.rock_paper_scissors.RockPaperScissors.OPTION.PAPER;
import static com.ebay.test.rock_paper_scissors.RockPaperScissors.OPTION.ROCK;
import static com.ebay.test.rock_paper_scissors.RockPaperScissors.OPTION.SCISSORS;
import static com.ebay.test.rock_paper_scissors.RockPaperScissors.calculate;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class RockPaperScissorsTest {

    @Parameterized.Parameters(name = "[{0} vs {1}: {3}] {2}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {ROCK, ROCK, "It's a tie", null},
                {PAPER, PAPER, "It's a tie", null},
                {SCISSORS, SCISSORS, "It's a tie", null},
                {ROCK, PAPER, "PAPER beats ROCK", PAPER},
                {PAPER, ROCK, "PAPER beats ROCK", PAPER},
                {ROCK, SCISSORS, "ROCK beats SCISSORS", ROCK},
                {SCISSORS, ROCK, "ROCK beats SCISSORS", ROCK},
                {PAPER, SCISSORS, "SCISSORS beats PAPER", SCISSORS},
                {SCISSORS, PAPER, "SCISSORS beats PAPER", SCISSORS},
                {null, PAPER, "Unable to determine winner", null},
                {PAPER, null, "Unable to determine winner", null},
                {null, null, "Unable to determine winner", null},
        });
    }

    private final OPTION option1;
    private final OPTION option2;
    private final String expectedResult;
    private final OPTION expectedWinner;

    public RockPaperScissorsTest(OPTION option1, OPTION option2, String expectedResult, OPTION expectedWinner) {
        this.option1 = option1;
        this.option2 = option2;
        this.expectedResult = expectedResult;
        this.expectedWinner = expectedWinner;
    }

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void paperShouldBeatRock() {
        OPTION winner = calculate(option1, option2);

        assertThat(systemOutRule.getLog()).contains(expectedResult);
        assertThat(winner).isEqualTo(expectedWinner);
    }
}
