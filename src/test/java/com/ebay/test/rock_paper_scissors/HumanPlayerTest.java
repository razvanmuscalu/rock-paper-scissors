package com.ebay.test.rock_paper_scissors;

import com.ebay.test.rock_paper_scissors.RockPaperScissors.OPTION;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.concurrent.CyclicBarrier;

import static com.ebay.test.rock_paper_scissors.RockPaperScissors.OPTION.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class HumanPlayerTest {

    @Mock
    private CyclicBarrier barrier;

    @Mock
    private InputAsker inputAsker;

    @InjectMocks
    private HumanPlayer sut;

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void shouldPlayRockWhenInputIs1() {
        given(inputAsker.getPlayerOption()).willReturn(1);

        OPTION result = sut.call();

        assertThat(result).isEqualTo(ROCK);
    }

    @Test
    public void shouldPlayPaperWhenInputIs2() {
        given(inputAsker.getPlayerOption()).willReturn(2);

        OPTION result = sut.call();

        assertThat(result).isEqualTo(PAPER);
    }

    @Test
    public void shouldPlayScissorsWhenInputIs3() {
        given(inputAsker.getPlayerOption()).willReturn(3);

        OPTION result = sut.call();

        assertThat(result).isEqualTo(SCISSORS);
    }

    @Test
    public void shouldNotPlayWhenInputIsNotValid() {
        given(inputAsker.getPlayerOption()).willReturn(-1);

        OPTION result = sut.call();

        assertThat(result).isNull();
    }

    @Test
    public void shouldInformThatHumanPlayed() {
        given(inputAsker.getPlayerOption()).willReturn(1);

        sut.call();

        assertThat(systemOutRule.getLog()).contains("null played");
    }
}
