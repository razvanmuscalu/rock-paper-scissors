# Rock, Paper, Scissors

## Solution overview

- command-line based application which acts based on user input
- user can specify length of game, as well as game mode (player vs computer or computer vs computer)
- user can specify which hand to play every time
  - however the game times out if user doesn't play a hand and the user looses that turn
- computer just randomly plays a hand each time
- the game keeps score and displays it at the end of the game
- the two players run in separate threads to better simulate a real environment
- the logic to determine the winner is fully extensible and should easily support lizard-spock extension

### Notes 

- `mvn exec:java -Dexec.mainClass="com.ebay.test.rock_paper_scissors.Main"` will run the game
- `mvn clean test` will run all tests
- There are also some IntelliJ run configurations if you import the project in IntelliJ.


==================================================================================================


## Requirements

_User Story Front_

```
+--------------------------------------------------+
|                                                  |
|     Title: Waste an Hour Having Fun              |
|                                                  |
| As a frequent games player,                      |
| I'd like to play rock, paper, scissors           |
| so that I can spend an hour of my day having fun |
|                                                  |
| Acceptance Criteria                              |
|  - Can I play Player vs Computer?                |
|  - Can I play Computer vs Computer?              |
|  - Can I play a different game each time?        |
|                                                  |
+--------------------------------------------------+
``` 
 
_User Story Back_
```
+--------------------------------------------------+
|                                                  |
| Technical Constraints                            |
|                                                  |
| - Doesn't necessarily need a flashy GUI. A simple|
|   UI is  fine. However the user experience will  |
|   be considered.                                 |
| - Use Java or Scala                              |
| - Libs / external modules should only be used    |
|   for tests                                      |
| - Using best in industry agile engineering       |
|   practices                                      |
|                                                  |
+--------------------------------------------------+
```
 
Don't know the game? http://en.wikipedia.org/wiki/Rock-paper-scissors

_Guidance_

We wouldn't ask anyone to do a coding puzzle that we haven't done ourselves, so the people looking at your code understand the problem we're asking to be solved. 
 
 - We’re not too bothered with the UI.  However, if you are mostly a front end engineer a nice UI is a requirement
 - We are keen to see how much you think is enough, and how much would go into a Minimum Viable Product.  As a guide, elegant and simple wins over feature rich every time, though extra gold stars are given to people who get excited and do more because they are having fun.
 - Do you test drive your code? This is something we value at eBay, and we will be looking for telling indicators of such in the code you produce.  We'll also be looking for things like coverage, copy and paste detection etc
 - We also like to see your ‘long division’ i.e. the working out of the problem in code
 - Run / build instructions are seen in a positive light, as it indicates you know how to work in that environment
 - We also consider the extensibility of the code produced.  Well factored code should be relatively easily extended. http://en.wikipedia.org/wiki/Rock-paper-scissors-lizard-Spock may be a natural extension.
 - We're happy for solutions in either Java or Scala
 - Any indicator of design (DDD, or design patterns) would make us smile.


==================================================================================================


## Feedback Notes

### Things we liked:

_Tests_

 - Test Coverage is high (~90%)
 - Test Naming is clear and easy to follow

_Git Log_

 - The git log was provided, and the commits had (mostly) descriptive (if sometimes short) commit messages
 - The git log shows that tests were committed with the change in functionality, rather than after-the-fact.

_Code Design_

 - The logic for determining the winner is neatly encapsulated inside a "beats" method for each object. This would make the logic for implementing RPSLS easier as each option only needs to know what it beats. (However, see below point on IO Separation)
 - Seems to have good knowledge of Java (Threads, JUnit etc)

_Readme included_

### Things that need improvement

_Tidiness of project_

 - The initial commit from the git history is quite large, which makes it hard to see if the initial MVP was developed using TDD.
 - Subsequent commits are not all granular. Some commits are still quite large.

_Game play is not always obvious_

 - When the user plays according to the happy path, game play is relatively smooth. However, when the user deviates, or 'misses a beat', it becomes confusing:
 - If a user waits too long before entering a move, it is not always obvious that they have timed out.
 - It is not always clear if the user has entered an invalid option into the game. Error messages are sporadic, and defaults are not consistent.
 - The default option for game play time does not match the message.
 - The length of the game is not indicated when the user starts playing the game (in seconds). The program just exits when the timeout is reached, rather than exiting at a sensible point (e.g. after a round)

_Code design_

 - There's no clear separation of Input and Output. "System.out.println"s are scattered at every layer within the code, as are requests for user inputs using the Scanner. Some attempt has been made to separate user input by providing an InputAsker, but this is not taken to it's logical conclusion.
 - Naming of OPTION to represent Rock, Paper, Scissors enum is inconsistent with the rest of the code, and not very descriptive. This was first encountered as 'Future OPTION ', which (if you are familiar with Scala/Guava options) can be unclear as to what is going on. Option is also not a good name for the move that is played, and a more descriptive name would be helpful.
 - Magic Numbers are spread throughout the code, and are inconsistent. In some places, magic numbers are extracted into fields, but this is again not followed through to it's logical conclusion.
 - No separation between Unit and Integration Tests, either in package name or class name. Both types of test are used in this project.
 - Long Running Unit Tests have been implemented for testing timeouts. These should be separated into integration tests, and maybe include adjustable timeout configuration. The purpose of these tests should be to show that the behaviour on timeout is correct, and a unit test could assert the desired length of timeout is correct in production code.
 - Testing the computer player really tests the distribution of the Random Number Generator. These could be refactored by using Dependency Injection to inject a generator into the computer player for generating moves, and providing a specific test implementation of the generator to assert that the right values are chosen for a move based on a known sequence.
 
 
## More Feedback Notes
 
 
 The intention of our test is to allow us to see what kind of code we will have to maintain if this candidate is working beside us. While your code demonstrated a number of good points, including great test coverage and good use of polymorphism, unfortunately it fell short of our high standards in terms of expressiveness and simplicity. 
 
 The use of int literals for concepts such as game mode, or the number of parties to the CyclicBarrier, make the code difficult to read and understand. We would prefer to see these replaced with constants, whose names describe what the value actually means.
 
 The decision to use multiple threads is also questionable, as the problem can be solved more simply without threads, however we understand that some candidates want to show their understanding of Java's concurrency API. Allowing for that, we still feel that the use of both a CyclicBarrier and a CountDownLatch is an unnecessarily complicated approach to concurrency control. Perhaps using a CountDownLatch alone, with each player thread counting it down and the main thread awaiting the latch, with a timeout, could have removed the need for both the Cyclic Barrier and the loop implemented in the countDown() method?  More expressive naming of those variables could also have made the intent of each more obvious. e.g. allPlayersHavePlayed.await(), rather than barrier.await().